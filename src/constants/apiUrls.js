import apiCommonParams from './apiCommonParams';
let baseURL = 'https://smark-service-qa1.sitepm.com/rest/v3';
let adminBaseURL = 'https://smark-admin-qa1.sitepm.com/rest/v3';

const apiUrls = {
    saveProject: `${baseURL}/smark/save-project`,
    saveRelease: `${baseURL}/smark/save-release`,
    deleteProject: `${baseURL}/smark/delete-project`,
    deleteRelease: `${baseURL}/smark/delete-release`,
    project: baseURL + "/smark/projectslisthb/getallprojects/applicationid/179/siteid/95/userid/{0}?swcmTicket=69f2ed48-6850-4e0f-a2e9-90b54c50fd28",
    release: baseURL + "/smark/projectrelaseshb/getallreleaseofaproject/applicationid/179/siteid/95/projectid/{0}/?swcmTicket=69f2ed48-6850-4e0f-a2e9-90b54c50fd28",
    users: adminBaseURL + "/user/userlisthb/applicationid/179/siteid/95/module/0/count/0?swcmTicket=69f2ed48-6850-4e0f-a2e9-90b54c50fd28",
    tasks: baseURL + "/smark-user-story/releasetasklisthb/getalltaskcommentsofauserinadaterange/applicationid/179/siteid/95/projectid/{0}/releaseid/{1}/userid/{2}/userstoryid/{3}/startdate/{4}/enddate/{5}/entitytype/tm_task/?swcmTicket=69f2ed48-6850-4e0f-a2e9-90b54c50fd28",

    stories: `${baseURL}/smark-user-story/releasetasklisthb/getalluserstoryofarelease/applicationid/${apiCommonParams.applicationid}/siteid/${apiCommonParams.siteid}/releaseid/{0}/?swcmTicket=${apiCommonParams.swcmTicket}`,
    taskEditStories: `${baseURL}/smark-user-story/userstorylisthb/getuserstorybysearch/applicationid/${apiCommonParams.applicationid}/siteid/${apiCommonParams.siteid}/projectid/{0}/keyword/{1}/?swcmTicket=${apiCommonParams.swcmTicket}`,
    saveStory: `${baseURL}/smark-user-story/save-user-story`,
    updateStoryPriority: `${baseURL}/smark-user-story/update-user-story-priority`,
    userStoryById: `${baseURL}/smark-user-story/userstorydetailshb/getdetailsofauserstory/applicationid/179/siteid/95/userstoryid/{0}?swcmTicket=69f2ed48-6850-4e0f-a2e9-90b54c50fd28`,
    addCommentToStory: `${baseURL}/smark-user-story/save-comments`,
    deleteStory: `${baseURL}/smark-user-story/delete-user-story`,
    storyComments: `${baseURL}/smark-user-story/userstorydetailshb/getdetailsofauserstory/applicationid/${apiCommonParams.applicationid}/siteid/${apiCommonParams.siteid}/userstoryid/{0}?swcmTicket=${apiCommonParams.swcmTicket}`,

    getTaskById: `${baseURL}/smark/taskcommentshb/getallcommentsofatask/applicationid/${apiCommonParams.applicationid}/siteid/${apiCommonParams.siteid}/taskid/{0}/?swcmTicket=${apiCommonParams.swcmTicket}`,
    addTask: `${baseURL}/smark/save-task`,
    updateTaskStatus: `${baseURL}/smark/update-task-status`,
    deleteTask: `${baseURL}/smark/delete-task`,
    addCommentToTask: `${baseURL}/smark/save-comments`,
    removeTaskAssignee: `${baseURL}/smark/delete-assignee`,
    loginPath: `${adminBaseURL}/user/user-login`,
    isAdminPath: `${adminBaseURL}/user/isadmin/applicationid/179/userid/{0}?swcmTicket=69f2ed48-6850-4e0f-a2e9-90b54c50fd28`,
    signUpPath: `${adminBaseURL}/user/user-register`,
    forgotPswd:`${adminBaseURL}/user/smark/reset-password`,
    editProject:`${baseURL}/smark/validate-projectname`,
    editRelease:`${baseURL}/smark/validate-releasename`
}

export default apiUrls;