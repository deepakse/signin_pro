import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './Signin.css';
import './common-style.css';
import './login.css';
//import { isValidEmail, passwordExpression } from '../../helper/utils';
import { toast } from 'react-toastify';
import { isAdminUser, loginUser } from '../../helper/context/actions';
import { useSmarkDispatch, useSmarkState } from '../../helper/context/context';
import { trackPromise } from 'react-promise-tracker';
import apiCommonParams from '../../constants/apiCommonParams';

const SignIn = () => {
    const dispatch = useSmarkDispatch();
    const { loading } = useSmarkState();

    const history = useHistory();
    const initialState = {
        email: '',
        password: ''
    };
    const [formState, setFormState] = useState({ ...initialState });
    const [formErrors, setFormErrors] = useState({
        email: '',
        password: ''
    });
    const { email, password } = formState;

    const [tncSate, setTncState] = useState(false);

    const onChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        setFormState({ ...formState, [name]: value });
        let errors = { ...formErrors }
        switch (name) {
            case 'email':
                errors.email = value.length < 1 ? 'This field is required!' : '';
                // errors.email = value.length < 1 ? 'This field is required!' : isValidEmail(value) ? '' : 'Email is not valid';
                break;
            case 'password':
                errors.password = value.length < 1 ? 'This field is required!' : '';
                // errors.password = value.length < 1 ? 'This field is required!' : passwordExpression(value) ? '' : 'Must contain at least one number, one Uppercase letter, one lowercase letter and one special character, and at least 8 characters';
                break;
            default:
                break;
        }
        setFormErrors({ ...formErrors, ...errors });
    }

    const isFormValid = () => {
        let valid = true;
        if (formState.email.length === 0 || formState.password.length === 0) {
            let errors = { ...formErrors }
            if (formState.email.length === 0) {
                errors.email = 'This field is required!';
            }

            if (formState.password.length === 0) {
                errors.password = 'This field is required!';
            }

            setFormErrors({ ...formErrors, ...errors });
            return false;
        }
        return valid;
    }

    const submitMethod = (event) => {
        event.preventDefault();
        if (isFormValid()) {
            let payload = {
                "username": formState.email,
                "password": formState.password,
                "domain": "smark-qa1.sitepm.com",
            };

            let formData = new FormData();
            formData.append("swcmTicket", apiCommonParams.swcmTicket);
            formData.append("jsonData", JSON.stringify(payload));

            trackPromise(
                loginUser(dispatch, formData).then((response) => {
                    if(response.enabled===false){
                        toast.warning('Please verify your account first!', {
                            position: toast.POSITION.TOP_RIGHT
                        });                      
                    }else{
                        if (response.userId) {
                            isAdminMethod(response);
                            toast.success('Login successfully!', {
                                position: toast.POSITION.TOP_RIGHT
                            });
                            history.push('/dashboard');
                        } else {
                            let error = response[0].errorMap;
                            let message = Object.values(error);
                            toast.error(message[0], {
                                position: toast.POSITION.TOP_RIGHT
                            });
                        } 
                    }
                }).catch((error) => {
                    toast.error(error, {
                        position: toast.POSITION.TOP_RIGHT
                    });
                })
            );
        }
    }

    const isAdminMethod = (userDetail) => {
        trackPromise(
            isAdminUser(dispatch, userDetail).then((response) => {
                if (response) { }
            }).catch((error) => {

            })
        );
    }

    const signupMethod = (event) => {
        event.preventDefault();
        history.push("/signup");
    }

    const forgotPwdMethod = (event) => {
        event.preventDefault();
        history.push("/forgot-password");
    }

    return (
        <div className="login-back login-top">
            <div className="row">
                <div className="container">
                    <div className="col-lg-10 col-md-12 col-md-offset-1 row-eq-height">
                        <div className="col-md-5 col-sm-12 col-xs-12 login-inner login-screen ">
                            <h2>Hello...</h2>
                            <p className="text-left login-P-text">Sign in to your account to have access to more features</p>
                            <hr />
                            <h4>Don't have an account?</h4>
                            <span onClick={signupMethod} className="account-button">
                                <i className="fa fa-user" aria-hidden="true"></i> Sign Up
                            </span>
                        </div>
                        <div className="col-md-7 col-sm-12 col-xs-12 account-create login-screen">
                            <h2>Sign In</h2>
                            <form name="userLogin" id="userLogin" method="post" className="mar-top-30">
                                <div className="form-group">
                                    <input type="email" name="email" value={email} className="form-control" placeholder="Username/Email" id="email" onChange={onChange} />
                                    {formErrors.email.length > 0 && <small className="text-danger">{formErrors.email}</small>}
                                </div>
                                <div className="form-group mt-4">
                                    <input type="password" name="password" value={password} className="form-control" id="pwd" onChange={onChange} placeholder="Password" />
                                    {formErrors.password.length > 0 && <small className="text-danger">{formErrors.password}</small>}
                                </div>
                                <div className="form-group mt-4">
                                    <input type="checkbox" name="tnc" id="tnc" checked={tncSate} onChange={() => setTncState(!tncSate)} />
                                    <label htmlFor="remember" className="ml-1">Remember Me</label>
                                </div>
                                <div className="form-group mt-2">
                                    <button type="submit" onClick={submitMethod} disabled={loading} className="btn btn-ar btn-primary mar-bot-30">Login</button>
                                </div>

                                <div className="col-md-6 nopad smark-user">
                                    <p className="text-left"><span onClick={forgotPwdMethod} className="forgot"> <i className="fa fa-lock" aria-hidden="true"></i> Forgot Password</span></p>
                                </div>
                                <div className="col-md-12">
                                    <img src={process.env.PUBLIC_URL + 'images/smark.png'} alt="Logo" width="150" className="img-responsive pull-right" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SignIn;
