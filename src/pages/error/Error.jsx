import React from 'react';

const Error = () => {
    return (
        <div>
            <h1 className="text-center text-danger mt-4">OOps! Page not found!</h1>
        </div>
    )
}

export default Error;