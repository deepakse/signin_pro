import React from 'react';
import './Dashboard.css';
import Header from '../../components/header/header';
import { useSmarkDispatch, useSmarkState } from '../../helper/context/context';

const Dashboard = () => {
    const contextDetails = useSmarkState();
    const dispatch = useSmarkDispatch();

    return (
        <div>
            <Header></Header>
        </div>
    )
}

export default Dashboard;
