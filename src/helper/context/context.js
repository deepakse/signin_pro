import React, { useReducer } from "react";
import { SmarkReducer, initialState } from "./reducer";

const SmarkContext = React.createContext();
const SmarkDispatchContext = React.createContext();

export function useSmarkState() {
  const context = React.useContext(SmarkContext);
  if (context === undefined) {
    throw new Error("useSmarkState must be used within a SmarkProvider");
  }

  return context;
}

export function useSmarkDispatch() {
  const context = React.useContext(SmarkDispatchContext);
  if (context === undefined) {
    throw new Error("useSmarkDispatch must be used within a SmarkProvider");
  }

  return context;
}

export const SmarkContextProvider = ({ children }) => {
  const [user, dispatch] = useReducer(SmarkReducer, initialState);

  return (
    <SmarkContext.Provider value={user}>
      <SmarkDispatchContext.Provider value={dispatch}>
        {children}
      </SmarkDispatchContext.Provider>
    </SmarkContext.Provider>
  );
};
