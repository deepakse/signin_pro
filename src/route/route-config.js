import SignIn from '../pages/signin/Signin';
import SignUp from '../pages/signUp/SignUp';
import ForgotPassword from '../pages/forgotPwd/ForgotPwd';
import Dashboard from '../pages/dashboard/Dashboard';

const CommonRoutes = [
    {
        path: '/',
        component: SignIn,
        exact: true
    },
    {
        path: '/signup',
        component: SignUp,
        exact: true,
    },
    {
        path: '/forgot-password',
        component: ForgotPassword,
        exact: true,
    }
];

const RestrictedRoutes = [
    {
        path: '/dashboard',
        component: Dashboard,
        exact: true
    }
];

export { CommonRoutes, RestrictedRoutes };
