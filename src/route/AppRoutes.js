import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { useSmarkState } from "../helper/context/context";
import { CommonRoutes, RestrictedRoutes } from "./route-config";

const AppRoutes = () => {
    const userDetails = useSmarkState();

    return (
        <Switch>
            {
                CommonRoutes.map((route, index) => {
                    const { component: Component, ...rest } = route;
                    return (
                        <Route {...rest} key={index} render={matchProps => {
                            if (!userDetails.userId) {
                                return (
                                    <Component {...matchProps} />
                                )
                            } else {
                                return (
                                    <Redirect to="/dashboard" key={index} />
                                )
                            }
                        }
                        } />
                    )
                })
            }

            {
                RestrictedRoutes.map((route, index) => {
                    const { component: Component, ...rest } = route;
                    return (
                        <Route {...rest} key={index} render={matchProps => {
                            if (userDetails.userId) {
                                return (
                                    <Component {...matchProps} />
                                )
                            } else {
                                return (
                                    <Redirect to="/" key={index} />
                                )
                            }
                        }
                        } />
                    )
                })
            }

            <Route path='*' exact={true} render={matchProps => <Redirect to="/" />} />
        </Switch>
    )
}

export default AppRoutes;